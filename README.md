# ACME USERS

API and client for listing users.

# Requirements:

Linux with Git, Docker, Docker-Compose, and Composer installed.
 
 
# Installation:

#### 1. Clone repository and run build:
   
```
cd /var/www

git clone git@bitbucket.org:ondrusek/cors.git cors-test

cd cors-test

./build.sh
```
   
#### 2. Import user fixtures in Adminer:

[http://localhost:8080/?server=mysql%3A3306&username=root&db=acme&import=](http://localhost:8080/?server=mysql%3A3306&username=root&db=acme&import=)

Password: ```root```

Select: ```./config/acme-db.sql```

#### 3. Add virtual host:

```sudo nano /etc/hosts```

Add line: ```127.0.0.1 acme.local```

#### 4. Open ```http://acme.local``` in browser

#### 5. Fixtures credentials:
   Usernames are top 200 Czech names sorted by frequency.
   Password is always same as username.
   E.g.: ```Adam/Adam``` or ```Eva/Eva```

# TODO
- Automatic loading of user fixtures
- Run composer from inside of container
