<?php

namespace App\Controller;

use App\Service\ApiService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $api;

    public function __construct(ApiService $api)
    {
        $this->api = $api;
    }

    /**
     * @Route("/", name="home")
     * @Template
     */
    public function homeAction(): RedirectResponse
    {
        return $this->redirectToRoute('users');
    }

    /**
     * @Route("/uzivatele/{offset}", name="users")
     * @Template
     */
    public function usersAction(int $offset = 1)
    {
        try {
            $responseData = $this->api->getUsers($offset);
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('login');
        }

        // Pagination:

        $count = $responseData['count'];
        $currentOffset = $responseData['offset'];

        $maxOffsets = ceil($count / 20);

        for ($i = 1; $i <= $maxOffsets; $i++) {
            $pagination[] = $i;
        }

        if ($currentOffset - 1 > 0) {
            $previousPage = $currentOffset - 1;
        } else {
            $previousPage = 1;
        }

        if ($currentOffset < $maxOffsets) {
            $nextPage = $currentOffset + 1;
        } else {
            $nextPage = $maxOffsets;
        }

        return [
            'users' => $responseData['users'],
            'pagination' => $pagination,
            'currentOffset' => $currentOffset,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
        ];
    }

    /**
     * @Route("/login", name="login")
     * @Template
     */
    public function loginAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('username', TextType::class,     ['label' => 'login_form.username'])
            ->add('password', PasswordType::class, ['label' => 'login_form.password'])
            ->add('submit',   SubmitType::class,   ['label' => 'login_form.submit']);

        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $responseSuccess = $this->api->login([
                'username' => $formData['username'],
                'password' => $formData['password'],
            ]);

            if ($responseSuccess === true) {
                return $this->redirectToRoute('home');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}