<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiService
{
    private $client;
    private $session;
    private $flash;
    private $apiUrl;

    public function __construct(HttpClientInterface $client, SessionInterface $session, FlashBagInterface $flash, string $apiUrl)
    {
        $this->client = $client;
        $this->session = $session;
        $this->flash = $flash;
        $this->apiUrl = $apiUrl;
    }

    function getFromAPI(string $path): ?array
    {
        if (!$this->session->has('jwt')) {
            throw new \Exception('login_form.not_logged_in');
        }

        $apiResponse = $this->client->request(
            'GET',
            'http://' . $this->apiUrl . '/api/' . $path,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->session->get('jwt'),
                ],
            ]
        );

        if ($apiResponse->getStatusCode() != 200) {
            throw new \Exception('Error: ' . $apiResponse->getStatusCode());
        }

        return json_decode($apiResponse->getContent(), true);
    }

    function getUsers(int $offset = 1): ?array
    {
        return $this->getFromAPI('users/' . $offset);
    }

    function login(array $credentials): bool
    {
        $response = $this->client->request(
            'POST',
            'http://' . $this->apiUrl . '/api/login_check',
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'username' =>  $credentials['username'],
                    'password' => $credentials['password'],
                ],
            ]
        );

        if ($response->getStatusCode() == 200) {
            $token = json_decode($response->getContent(), true)['token'];

            $this->session->set('jwt', $token);

            return true;
        }

        if ($response->getStatusCode() == 401) {
            $this->flash->add('error', 'login_form.not_logged_in');
        } else {
            $this->flash->add('error', 'Error: ' . $response->getStatusCode());
        }

        return false;
    }
}