<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $names = explode("\n", file_get_contents(__DIR__ . '/czech-names-by-frequency.txt'));

        foreach ($names as $name)
        {
            $user = new User;

            $user->setUsername($name);
            $user->setPassword($this->encoder->encodePassword($user, $name));

            $manager->persist($user);
        }

        $manager->flush();
    }
}
