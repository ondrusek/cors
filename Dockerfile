FROM php:7.4-apache

RUN mkdir -p /var/www/cors/application

COPY ./application /var/www/cors/application

COPY ./config/acme.conf /etc/apache2/sites-available/acme.conf
COPY ./config/acme-api.conf /etc/apache2/sites-available/acme-api.conf
COPY ./config/hosts /etc/hosts

RUN a2enmod rewrite

RUN a2ensite acme
RUN a2ensite acme-api


ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions mysqli pdo pdo_mysql zip


#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#WORKDIR /var/www/cors/application/client
#RUN composer install
#WORKDIR /var/www/cors/application/server
#RUN composer install
