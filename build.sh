#!/bin/sh

cd ./application/client
mkdir ./var ./vendor
chmod -R 777 ./var ./vendor
composer install

cd ../server
mkdir ./var ./vendor
chmod -R 777 ./var ./vendor
composer install

cd ../..
docker-compose up --build
